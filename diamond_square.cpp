#include <cstdlib>
#include <cstdio>
#include <algorithm>
#include <boost/numeric/ublas/matrix_proxy.hpp>
#include "diamond_square.hpp"

#include <iostream>

using namespace std;
using namespace boost::numeric::ublas;
using namespace glm;


double rand_0_1()
{
    return (double) rand() / RAND_MAX;
}

double myrand()
{
    return rand_0_1() * 2. - 1.;
}

double average(double a, double b, double c, double d)
{
    return (a + b + c + d) / 4.;
}

matrix<float> get_hmap(size_t iter, double r)
{
    size_t sz = (1 << iter);
    //size_t mask = ~sz;
    double h = 1.;

    matrix<float> hmap (sz, sz);
    fill(hmap.begin1(), hmap.end1(), 0.f);


    // set hmap(0,0) ?
    for (size_t space = sz; space > 1; space /= 2)
    {
        size_t mid = space / 2;

        // CARRE
        for (size_t x = mid; x < sz; x += space)
            for (size_t y = mid; y < sz; y += space)
            {
                const size_t xa = (x - mid) % sz, xb = (x + mid) % sz,
                             ya = (y - mid) % sz, yb = (y + mid) % sz;

                //cout << "carre center: (" << x << "," << y << ")" << endl;

                hmap(x,y) = myrand() * h + average(hmap(xa,ya), hmap(xa,yb), hmap(xb,ya), hmap(xb,yb));
            }

        // DIAMANT
        for (int x = 0; x < (int)sz; x += space)
            for (int y = mid; y < (int)sz; y += space)
            {
                const size_t xa = (x - mid) % sz, xb = (x + mid) % sz,
                             ya = (y - mid) % sz, yb = (y + mid) % sz;

                //cout << "diamond center: (" << x << "," << y << ")" << endl;

                hmap(x,y) = myrand() * h + average(hmap(xa,y), hmap(xb,y), hmap(x,ya), hmap(x,yb));
            }
        for (int x = mid; x < (int)sz; x += space)
            for (int y = 0; y < (int)sz; y += space)
            {
                const size_t xa = (x - mid) % sz, xb = (x + mid) % sz,
                             ya = (y - mid) % sz, yb = (y + mid) % sz;

                //cout << "diamond center: (" << x << "," << y << ")" << endl;

                hmap(x,y) = myrand() * h + average(hmap(xa,y), hmap(xb,y), hmap(x,ya), hmap(x,yb));
            }

        h *= r;
    }

    // normalize hmap between [0.,1.)
    auto min_max = minmax_element(hmap.data().begin(), hmap.data().end());
    float mini = *min_max.first, maxi = *min_max.second;
    //cout << "mini=" << mini << ", maxi=" << maxi << ", rg=" << (maxi - mini) << endl;

    hmap -= scalar_matrix<float>(hmap.size1(), hmap.size2(), mini);
    hmap *= 1.f / (maxi - mini);

    return hmap;
}

void write_pgm(const char* filename, matrix<float>& hmap)
{
    FILE *fp = fopen(filename, "w");
    fprintf(fp, "P2\n%zu\n%zu\n255\n", hmap.size2(), hmap.size1());
    for (unsigned i = 0; i < hmap.size1 (); ++ i)
    {
        matrix_row<matrix<float>> mr (hmap, i);
        for (auto it = mr.begin(); it != mr.end(); ++it)
        {
            fprintf(fp, "%u ", (unsigned)(*it * 255));
        }
        fprintf(fp, "\n");
    }
    fclose(fp);
}

vec3 triangle_normal(const triangle_t& tri)
{
    vec3 edge1 = tri.v2 - tri.v1, edge2 = tri.v3 - tri.v1;
    return normalize(cross(edge1, edge2));
}

std::vector<vec3> vertices;
std::vector<vec3> normals;
std::vector<uvec3> triangles_idx;
std::vector<unsigned short> indices;

void set_data(matrix<float>& _hmap, float upscale)
{
    // redim matrix
    const size_t _sz1 = _hmap.size1(), _sz2 = _hmap.size2();
    matrix<float> hmap(_sz1 + 1, _sz2 + 1);
    matrix_range<matrix<float>> rg(hmap, range(0, _sz1), range(0, _sz2));
    rg = matrix_range<matrix<float>> (_hmap, range(0, _sz1), range(0, _sz2));
    matrix_range<matrix<float>> col(hmap, range(_sz1, _sz1+1), range(0, _sz2));
    col = matrix_range<matrix<float>> (_hmap, range(0, 1), range(0, _sz2));
    matrix_range<matrix<float>> row(hmap, range(0, _sz1), range(_sz2, _sz2+1));
    row = matrix_range<matrix<float>> (_hmap, range(0, _sz1), range(0, 1));
    hmap(_sz1, _sz2) = _hmap(0,0);

    const size_t sz1 = hmap.size1(), sz2 = hmap.size2();

    for (size_t x = 0; x < sz1; ++x)
        for (size_t y = 0; y < sz2; ++y)
            vertices.push_back(vec3(float(x), hmap(x,y) * upscale, float(y)));

    std::vector<std::vector<vec3>> norms(sz1, std::vector<vec3>(sz2));
    for (int x = 0; x < (int)sz1; ++x)
        for (int y = 0; y < (int)sz2; ++y)
        {
            int x1 = (x-1), x2 = (x+1), y1 = (y-1), y2 = (y+1);
            vec3 v(float(x), hmap(x,y) * upscale, float(y));
            vec3 v1(float(x1), hmap(x1%sz1,y) * upscale, float(y));
            vec3 v2(float(x2), hmap(x2%sz1,y) * upscale, float(y));
            vec3 v3(float(x), hmap(x,y1%sz2) * upscale, float(y1));
            vec3 v4(float(x), hmap(x,y2%sz2) * upscale, float(y2));

            triangle_t t1(v3,v,v1);
            triangle_t t2(v,v4,v1);
            triangle_t t3(v2,v4,v);
            triangle_t t4(v3,v2,v);

            vec3 vnormal = normalize(triangle_normal(t1) + triangle_normal(t2) + triangle_normal(t4) + triangle_normal(t4));

            vertices.push_back(v);
            normals.push_back(vnormal);
        }

    for (int x = 0, i = 0; x + 1 < (int)sz1; ++x)
        for (int y = 0; y < (int)sz2; ++y, ++i)
        {
            if (y + 1 < (int)sz2)
            {
                /*
                                triangles_idx.push_back(uvec3(i+1, i, i+sz2));
                                indices.push_back(i+1);
                                indices.push_back(i);
                                indices.push_back(i+sz2);
                */
                if ((y % 2) ^ (x % 2))
                {
                    triangles_idx.push_back(uvec3(i, i+1, i+sz2));
                    indices.push_back(i);
                    indices.push_back(i+1);
                    indices.push_back(i+sz2);
                }
                else
                {
                    triangles_idx.push_back(uvec3(i, i+sz2+1, i+sz2));
                    indices.push_back(i);
                    indices.push_back(i+sz2+1);
                    indices.push_back(i+sz2);
                }

            }
            if (y > 0)
            {
                /*
                                triangles_idx.push_back(uvec3(i, i+sz2-1, i+sz2));
                                indices.push_back(i);
                                indices.push_back(i+sz2-1);
                                indices.push_back(i+sz2);
                */
                if ((y % 2) ^ (x % 2))
                {
                    triangles_idx.push_back(uvec3(i, i+sz2, i-1));
                    indices.push_back(i);
                    indices.push_back(i+sz2);
                    indices.push_back(i-1);
                }
                else
                {
                    triangles_idx.push_back(uvec3(i, i+sz2, i+sz2-1));
                    indices.push_back(i);
                    indices.push_back(i+sz2);
                    indices.push_back(i+sz2-1);
                }
            }
        }
    /*
        normals.resize(vertices.size());
        for (auto it = triangles_idx.begin(); it != triangles_idx.end(); ++it)
        {
            size_t a = it->x, b = it->y, c = it->z;
            vec3 v1 = vertices[a], v2 = vertices[b], v3 = vertices[c];
            vec3 normal = triangle_normal(triangle_t(v1, v2, v3));
            normals[a] = normalize(normals[a] + normal);
            normals[b] = normalize(normals[b] + normal);
            normals[c] = normalize(normals[c] + normal);
        }
    */
}

void write_obj(const char* filename)
{
    FILE *fp = fopen(filename, "w");
    for (auto it = vertices.begin(); it != vertices.end(); ++it)
        fprintf(fp, "v %f %f %f\n", it->x, it->y, it->z);

    for (auto it = normals.begin(); it != normals.end(); ++it)
        fprintf(fp, "vn %f %f %f\n", it->x, it->y, it->z);

    for (auto it = triangles_idx.begin(); it != triangles_idx.end(); ++it)
        //fprintf(fp, "f %u %u %u\n", it->x+1, it->y+1, it->z+1);
        fprintf(fp, "f %u//%u %u//%u %u//%u\n", it->x+1, it->x+1, it->y+1, it->y+1, it->z+1, it->z+1);
    fclose(fp);
}
