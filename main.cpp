#include <iostream>
#include <ctime>
#include <cstdio>
#include <cstdlib>
#include <vector>

// Include GLEW
#include <GL/glew.h>

// Include GLFW
#include <GL/glfw.h>

// Include GLM
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include "shader.hpp"
#include "diamond_square.hpp"

using namespace std;
using namespace glm;


int main()
{
    using namespace boost::numeric::ublas;

    srand(time(NULL));

    size_t iter = 8;
    float mid((1 << iter) / 2);
    float hauteur(mid * 1./ 3.);
    float r = 0.55f;
    matrix<float> hmap = get_hmap(iter, r);
    set_data(hmap, hauteur);

    write_obj("test5.obj");

    cout << hmap.size1() << ',' << hmap.size2() << endl;
    cout << vertices.size() << " vertices, " << normals.size() << " normals" << endl;
    cout << indices.size() << " indices" << endl;
    cout << "hauteur max: " << hauteur << endl;

    //exit(0);

    // Initialise GLFW
    if( !glfwInit() )
    {
        fprintf( stderr, "Failed to initialize GLFW\n" );
        return -1;
    }

    glfwOpenWindowHint(GLFW_FSAA_SAMPLES, 4);
    glfwOpenWindowHint(GLFW_OPENGL_VERSION_MAJOR, 3);
    glfwOpenWindowHint(GLFW_OPENGL_VERSION_MINOR, 3);
    glfwOpenWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

    // Open a window and create its OpenGL context
    if( !glfwOpenWindow( 1024, 768, 0,0,0,0, 32,0, GLFW_WINDOW ) )
    {
        fprintf( stderr, "Failed to open GLFW window. If you have an Intel GPU, they are not 3.3 compatible. Try the 2.1 version of the tutorials.\n" );
        glfwTerminate();
        return -1;
    }

    // Initialize GLEW
    glewExperimental = true; // Needed for core profile
    if (glewInit() != GLEW_OK)
    {
        fprintf(stderr, "Failed to initialize GLEW\n");
        return -1;
    }

    // Dark blue background
    glClearColor(0.0f, 0.0f, 0.4f, 0.0f);

    // Enable depth test
    glEnable(GL_DEPTH_TEST);
    // Accept fragment if it closer to the camera than the former one
    glDepthFunc(GL_LESS);

    // Cull triangles which normal is not towards the camera
    glEnable(GL_CULL_FACE);

    GLuint VertexArrayID;
    glGenVertexArrays(1, &VertexArrayID);
    glBindVertexArray(VertexArrayID);

    // Create and compile our GLSL program from the shaders
    GLuint programID = LoadShaders( "shader.vert", "shader.frag" );

    // Get a handle for our "MVP" uniform
    GLuint MatrixID = glGetUniformLocation(programID, "MVP");
	GLuint ViewMatrixID = glGetUniformLocation(programID, "V");
	GLuint ModelMatrixID = glGetUniformLocation(programID, "M");

    // Load it into a VBO
    GLuint vertexbuffer;
    glGenBuffers(1, &vertexbuffer);
    glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
    glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(glm::vec3), &vertices[0], GL_STATIC_DRAW);

	GLuint normalbuffer;
	glGenBuffers(1, &normalbuffer);
	glBindBuffer(GL_ARRAY_BUFFER, normalbuffer);
	glBufferData(GL_ARRAY_BUFFER, normals.size() * sizeof(glm::vec3), &normals[0], GL_STATIC_DRAW);

	// Generate a buffer for the indices as well
	GLuint elementbuffer;
	glGenBuffers(1, &elementbuffer);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, elementbuffer);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.size() * sizeof(unsigned short), &indices[0] , GL_STATIC_DRAW);

    // Projection matrix : 45° Field of View, 4:3 ratio, display range : 0.1 unit <-> 100 units
    glm::mat4 Projection = glm::perspective(45.0f, 4.0f / 3.0f, 0.1f, float(mid * 2.5));

    // Model matrix : an identity matrix (model will be at the origin)
    glm::mat4 Model = glm::mat4(1.0f);

    glm::vec3 position = glm::vec3(mid, hauteur, mid);

    // Initial horizontal angle : toward -Z
    float horizontalAngle = 0.f;

    // Initial vertical angle : none
    float verticalAngle = 0.f;

    GLuint LightID = glGetUniformLocation(programID, "LightPosition_worldspace");
    glm::vec3 lightPos = glm::vec3(mid, 10. * hauteur, mid);
    glUniform3f(LightID, lightPos.x, lightPos.y, lightPos.z);

    // Use our shader
    glUseProgram(programID);

    do
    {
        //cout << "position :(" << position.x << "," << position.z << "," << position.y << ")" << endl;

        // Direction : Spherical coordinates to Cartesian coordinates conversion
        glm::vec3 direction(
            cos(verticalAngle) * sin(horizontalAngle),
            sin(verticalAngle),
            cos(verticalAngle) * cos(horizontalAngle)
        );

        // Camera matrix
        glm::mat4 View = glm::lookAt(
                             position,
                             position + direction,
                             glm::vec3(0,1,0)  // Head is up (set to 0,-1,0 to look upside-down)
                         );

        // Our ModelViewProjection : multiplication of our 3 matrices
        glm::mat4 MVP = Projection * View * Model; // Remember, matrix multiplication is the other way around

        // Clear the screen
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        // Send our transformation to the currently bound shader,
        // in the "MVP" uniform
        glUniformMatrix4fv(MatrixID, 1, GL_FALSE, &MVP[0][0]);
		glUniformMatrix4fv(ModelMatrixID, 1, GL_FALSE, &Model[0][0]);
		glUniformMatrix4fv(ViewMatrixID, 1, GL_FALSE, &View[0][0]);

        // 1rst attribute buffer : vertices
        glEnableVertexAttribArray(0);
        glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
        glVertexAttribPointer(
            0,                  // attribute
            3,                  // size
            GL_FLOAT,           // type
            GL_FALSE,           // normalized?
            0,                  // stride
            (void*)0            // array buffer offset
        );

		// 3rd attribute buffer : normals
		glEnableVertexAttribArray(1);
		glBindBuffer(GL_ARRAY_BUFFER, normalbuffer);
		glVertexAttribPointer(
			1,                                // attribute
			3,                                // size
			GL_FLOAT,                         // type
			GL_FALSE,                         // normalized?
			0,                                // stride
			(void*)0                          // array buffer offset
		);

		// Index buffer
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, elementbuffer);

		// Draw the triangles !
		glDrawElements(
			GL_TRIANGLES,      // mode
			indices.size(),    // count
			GL_UNSIGNED_SHORT,   // type
			(void*)0           // element array buffer offset
		);

        glDisableVertexAttribArray(0);
        glDisableVertexAttribArray(1);

        // Swap buffers
        glfwSwapBuffers();


        if (glfwGetKey(GLFW_KEY_PAGEDOWN) == GLFW_PRESS)
            position += glm::vec3(0.f,1.5f,0.f);
        if (glfwGetKey(GLFW_KEY_PAGEUP) == GLFW_PRESS)
            position -= glm::vec3(0.f,1.5f,0.f);

        if (glfwGetKey(GLFW_KEY_KP_ADD) == GLFW_PRESS)
            position += direction * 0.5f;
        if (glfwGetKey(GLFW_KEY_KP_SUBTRACT) == GLFW_PRESS)
            position -= direction * 0.5f;

        if (glfwGetKey(GLFW_KEY_UP) == GLFW_PRESS)
            verticalAngle -= 0.02f;
        if (glfwGetKey(GLFW_KEY_DOWN) == GLFW_PRESS)
            verticalAngle +=  0.02f;

        if (glfwGetKey(GLFW_KEY_RIGHT) == GLFW_PRESS)
            horizontalAngle -= 0.045f;
        if (glfwGetKey(GLFW_KEY_LEFT) == GLFW_PRESS)
            horizontalAngle += 0.045f;

        position += direction * 1.5f;

        glfwSleep(0.05);

    } // Check if the ESC key was pressed or the window was closed
    while( glfwGetKey( GLFW_KEY_ESC ) != GLFW_PRESS &&
            glfwGetWindowParam( GLFW_OPENED ) );

    //write_pgm("test3.pgm", hmap);
    //set_data(hmap, 65.f);
    //write_obj("test3.obj");

    // Cleanup VBO and shader
    glDeleteBuffers(1, &vertexbuffer);
	glDeleteBuffers(1, &normalbuffer);
	glDeleteBuffers(1, &elementbuffer);
    glDeleteProgram(programID);
    glDeleteVertexArrays(1, &VertexArrayID);

///////////////////////////////

    // Close OpenGL window and terminate GLFW
    glfwTerminate();

    return 0;
}
