#include <vector>
#include <glm/glm.hpp>
#include <boost/numeric/ublas/matrix.hpp>


struct triangle_t
{
    triangle_t(const glm::vec3& a, const glm::vec3& b, const glm::vec3& c) :
        v1(a), v2(b), v3(c)
    {}
    glm::vec3 v1, v2, v3;
};

extern std::vector<glm::vec3> vertices;
extern std::vector<glm::vec3> normals;
extern std::vector<unsigned short> indices;

boost::numeric::ublas::matrix<float> get_hmap(size_t iter, double r=0.5);
void write_pgm(const char* filename, boost::numeric::ublas::matrix<float>& hmap);
void set_data(const boost::numeric::ublas::matrix<float>& hmap, float upscale=80.f);
void write_obj(const char* filename);
